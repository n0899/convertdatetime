<?php

class ConvertDateTime
{
    private $time = false;

    public function __construct($date = false)
    {
        if($date)
        {
            if(ctype_digit($date))
            {
                $this->time = $date;
            }
            elseif(strtotime($date))
            {
                $this->time = strtotime($date);
            }
            else
            {
                $this->time = self::parseDate($date);
            }

            if(!$this->time)
            {
                echo 'Invalid date';
            }
        }
    }

    public function parseDate($date)
    {
        //work
        return false;
    }

    public function getCurrentDate()
    {
        return ($this->time) ? date("d.m.Y H:m:s", $this->time) : false;
    }
}